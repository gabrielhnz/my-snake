const STATE_RUNNING = 1;
const STATE_LOSING = 2;

// grosor snake
const SQUARE_SIZE = 20;

// tamaño tablero
const BOARD_WIDTH = 25;
const BOARD_HEIGHT = 25;

// default state
const DEFAULT_STATE = {
  canvas: null,
  context: null,
  count: 0,
  snake: [{ x: 0, y: 0 }],
  direction: { x: 1, y: 0 },
  prey: { x: 0, y: 0 },
  growing: 0,
  level: 1,
  speed: 185,
  runState: STATE_RUNNING,
};

// initial width sneak
const GROW_SCALE = 1;
const DIRECTIONS_MAP = {
  A: [-1, 0],
  D: [1, 0],
  S: [0, 1],
  W: [0, -1],
  a: [-1, 0],
  d: [1, 0],
  s: [0, 1],
  w: [0, -1],
  ArrowLeft: [-1, 0],
  ArrowRight: [1, 0],
  ArrowDown: [0, 1],
  ArrowUp: [0, -1],
};

let state = { ...DEFAULT_STATE };

window.onload = function () {
  state.canvas = document.querySelector("canvas");
  state.context = state.canvas.getContext("2d");

  window.onkeydown = function (e) {
    const direction = DIRECTIONS_MAP[e.key];

    if (direction) {
      const [x, y] = direction;
      if (-x !== state.direction.x && -y !== state.direction.y) {
        state.direction.x = x;
        state.direction.y = y;
      }
    }
  };

  tick();
};
