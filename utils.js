// fuction for random show snake and point
function randomXY() {
  return {
    x: parseInt(Math.random() * BOARD_WIDTH),
    y: parseInt(Math.random() * BOARD_HEIGHT),
  };
}

function detectCollision(state) {
  const head = state.snake[0];
  if (
    head.x < 0 ||
    head.x >= BOARD_WIDTH ||
    head.y >= BOARD_HEIGHT ||
    head.y < 0
  )
    return true;

  for (var idx = 1; idx < state.snake.length; idx++) {
    const sq = state.snake[idx];
    if (sq.x === head.x && sq.y === head.y) return true;
  }

  return false;
}