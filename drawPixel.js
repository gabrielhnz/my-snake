// Function for draw snake and point
function drawPixel(state, color, x, y) {
  state.context.fillStyle = color;
  state.context.fillRect(
    x * SQUARE_SIZE,
    y * SQUARE_SIZE,
    SQUARE_SIZE,
    SQUARE_SIZE
  );
}

// create apple in canvas
function drawAppleIcon(state, id, x, y, w = SQUARE_SIZE, h = SQUARE_SIZE) {
  const img = document.getElementById(id);
  state.context.drawImage(img, x * SQUARE_SIZE, y * SQUARE_SIZE, w, h);
}

function drawReactGradient(state, x, y) {
  let gradienteSnake = state.context.createLinearGradient(400, 0, 0, 0);
  gradienteSnake.addColorStop(0, "rgb(78,124,246)");
  gradienteSnake.addColorStop(1, "#A706C3");
  state.context.fillStyle = gradienteSnake;
  state.context.fillRect(
    x * SQUARE_SIZE,
    y * SQUARE_SIZE,
    SQUARE_SIZE,
    SQUARE_SIZE
  );
}

// Paint the snake and point in the canvas
function draw(state) {
  state.context.clearRect(0, 0, 500, 500);

  for (var idx = 0; idx < state.snake.length; idx++) {
    const { x, y } = state.snake[idx];
    if (idx === 0) drawAppleIcon(state, "snake-head", x-0.1, y-0.1, 25, 25);
    else drawReactGradient(state, x, y);
  }

  const { x, y } = state.prey;
  drawAppleIcon(state, "apple-icon", x, y);
}
