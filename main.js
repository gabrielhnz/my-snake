function tick() {
  const head = state.snake[0];
  const dx = state.direction.x;
  const dy = state.direction.y;
  const highestIndex = state.snake.length - 1;
  let tail = {};
  let speed = state.speed;

  const levelCount = document.getElementById("level-count");
  const countApple = document.getElementById("count-apple");
  countApple.innerHTML = `${state.count - 1}`;
  levelCount.innerHTML = `${state.level}`;

  Object.assign(tail, state.snake[state.snake.length - 1]);
  let didScore = head.x === state.prey.x && head.y === state.prey.y;

  if (state.runState === STATE_RUNNING) {
    for (let idx = highestIndex; idx > -1; idx--) {
      const sq = state.snake[idx];

      if (idx === 0) {
        sq.x += dx;
        sq.y += dy;
      } else {
        sq.x = state.snake[idx - 1].x;
        sq.y = state.snake[idx - 1].y;
      }
    }
  } else if (state.runState === STATE_LOSING) {
    speed = 10;

    if (state.snake.length > 0) state.snake.splice(0, 1);

    if (state.snake.length === 0) {
      state.runState = STATE_RUNNING;
      state.snake.push(randomXY());
      state.prey = randomXY();
    }
  }

  if (detectCollision(state)) {
    console.log("reset values");
    state = {
      ...DEFAULT_STATE,
      prey: state.prey,
      canvas: state.canvas,
      context: state.context,
      runState: STATE_LOSING,
      count: 1,
    };
  }

  if (didScore) {
    console.log("Snake eat apple");
    state.count++;
    state.growing += GROW_SCALE;
    state.prey = randomXY();
  }

  if (state.growing > 0) {
    state.snake.push(tail);
    state.growing -= 1;
  }

  requestAnimationFrame(() => draw(state));
  // change levels from count
  levels(state);

  setTimeout(tick, speed);
}
