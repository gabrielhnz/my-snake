function levels(state) {
  switch (state.count - 1) {
    case 10:
      state.level = 2;
      state.speed = 180;
      break;

    case 20:
      state.level = 3;
      state.speed = 160;
      break;

    case 30:
      state.level = 4;
      state.speed = 140;
      break;

    case 38:
      state.level = 5;
      state.speed = 120;
      break;

    case 45:
      state.level = 6;
      state.speed = 100;
      break;

    case 50:
      state.level = 7;
      state.speed = 80;
      break;

    case 55:
      state.level = 8;
      state.speed = 70;
      break;

    case 60:
      state.level = 9;
      state.speed = 60;
      break;

    case 65:
      state.level = 10;
      state.speed = 50;
      break;

    default:
      break;
  }
}
